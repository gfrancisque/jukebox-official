package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class GuismetFrancisque1_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	ArrayList<Song> beatlesTracks = new ArrayList<Song>();
    TheBeatles theBeatlesBand = new TheBeatles();
	
    beatlesTracks = theBeatlesBand.getBeatlesSongs();
	
	playlist.add(beatlesTracks.get(0));
	playlist.add(beatlesTracks.get(1));
	
	ImagineDragons imagineDragonsBand = new ImagineDragons();
	ArrayList<Song> imagineDragonsTracks = new ArrayList<Song>();
    imagineDragonsTracks = imagineDragonsBand.getImagineDragonsSongs();
	
	playlist.add(imagineDragonsTracks.get(0));
	playlist.add(imagineDragonsTracks.get(1));
	
	Journey journeyBand = new Journey();
	ArrayList<Song> journeyTracks = new ArrayList<Song>();
	journeyTracks = journeyBand.getJourneysSongs();
	
	playlist.add(journeyTracks.get(0));
	playlist.add(journeyTracks.get(1));
	
	Coldplay coldplayBand = new Coldplay(); // Added from classmate, Marie Igoe's code //
	ArrayList<Song> coldplayTracks = new ArrayList<Song>(); // Added from classmate, Marie Igoe's code //
	coldplayTracks = coldplayBand.getColdplaySongs(); // Added from classmate, Marie Igoe's code //
	
	playlist.add(coldplayTracks.get(0)); // Added from classmate, Marie Igoe's code //
	playlist.add(coldplayTracks.get(1)); // Added from classmate, Marie Igoe's code //
	playlist.add(coldplayTracks.get(2)); // Added from classmate, Marie Igoe's code //

	
	
    return playlist;
	}
}
