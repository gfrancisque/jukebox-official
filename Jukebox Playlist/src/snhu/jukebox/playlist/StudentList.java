package snhu.jukebox.playlist;

import snhu.student.playlists.*;

import java.util.ArrayList;
import java.util.List;

public class StudentList {

	public StudentList(){
	}

	public List<String> getStudentsNames() {
		ArrayList<String> studentNames = new ArrayList<String>();
		
		String GuismetFrancisque = "Guismet Francisque";
		studentNames.add(GuismetFrancisque);
		
		String StudentName2 = "TestStudent2Name";
		studentNames.add(StudentName2);
		
		//Module 5 Code Assignment
		//Add your name to create a new student profile
		//Use template below and put your name in the areas of 'StudentName'
		//String StudentName3 = "TestStudent3Name";
		//studentNames.add(StudentName3);
		
		return studentNames;
	}

	public Student GetStudentProfile(String GuismetFrancisque){
		Student emptyStudent = null;
	
		switch(GuismetFrancisque) {
		   case "TestStudent1_Playlist":
			   TestStudent1_Playlist testStudentPlaylist1 = new TestStudent1_Playlist();
			   Student TestStudent1 = new Student("TestStudent1", testStudentPlaylist1.StudentPlaylist());
			   return TestStudent1;
			   
		   case "TestStudent2_Playlist":
			   TestStudent2_Playlist testStudentPlaylist2 = new TestStudent2_Playlist();
			   Student TestStudent2 = new Student("TestStudent2", testStudentPlaylist2.StudentPlaylist());
			   return TestStudent2;
	
			// Module 6 Code Assignment - test case inclusion
		   case "GuismetFrancisque_Playlist":
			   GuismetFrancisque_Playlist guismetFrancisquePlaylist = new GuismetFrancisque_Playlist();
			   Student GuismetFrancisque1 = new Student("GuismetFrancisque", guismetFrancisquePlaylist.StudentPlaylist());
			   return GuismetFrancisque1;
		}
		return emptyStudent;
	}
}
